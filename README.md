# Qt GUI-Application Template

This project template is meant to be used as a starting point for any great Qt gui app that is in your mind.
It comes with barely nothing but a dummy two-button-one-text-field window but... But? But is CMake based and thus is compatible with JetBrain's CLion IDE for C/C++ development and you don't have to use the awful Qt Creator for code development anymore!

## Project setup

Before you start, make sure you have Qt 5.x installes correctly on your computer. Also, git might be a good idea to be installed by now :-)...

##### Clone project

To use this template as your new app starting point you may clone the latest version:
```
git clone --depth 1 -b master https://gitlab.com/maxgabler/qt-gui-app-template.git
```
I hereby assume you wouldn't be interested in to much of the commits to your own project code from the templates creation process...

#### Environment variables

Setup the following environment variables on your computer:
```
QT5_HOME=<Qt5 install dir>\<Version>\<compiler package>
CMAKE_PREFIX_PATH=%QT5_HOME%\lib\cmake
```
`<Qt5 install dir>` is the directory you choose in the Qt installer. On Windows the default is 'C:\Qt'. Inside the install directory Qt creates a subfolder named by the version of your Qt installation like '5.9.2'.
In a multiversion environment it may look like 'Qt5.9.2\5.9.2'. Reaplace `<version>` with either the first or the second pattern depending on your setup of Qt.
As `<compiler package>` use the precompiled package for the compiler of your choice.

Example (in admin cli on Windows):
```
setx -m QT5_HOME=C:\Qt\5.9.2\mingw53_32
setx -m CMAKE_PREFIX_PATH=%QT5_HOME%\lib\cmake
```

#### CLion IDE

1. Open project in CLion
2. Go to File -> Settings -> Build, Execution, Deployment -> Toolchains and set the 'MinGW Home' path. It should point to the MinGW installation inside the Qt installation path. In my case its `C:\Qt\Tools\mingw530_32`. If MinGW was recegnised the version will be diplayed beneath the input field. 
3. Change to the CMake section under 'Build, Execution, Deployment' and choose 'debug' as 'Generation path'. You may also choose a different path outside of the project directory. Than click 'OK' to close the settings window.